package ir.esafar.interview.interview.reactive.live.service.thirdparty;

import ir.esafar.interview.interview.reactive.live.model.thirdparty.IranAirEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Mohammad Yasin Sadeghi
 */
@Service
public class IranAirWebService implements ThirdPartyService<IranAirEntity> {

    @Override
    public List<IranAirEntity> getFlightList() {
        List<IranAirEntity> iranAirEntities = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            IranAirEntity entity = new IranAirEntity();
            entity.setCompany("Iran Air");
            entity.setFlight("flight " + i);
            entity.setId(Long.valueOf(1000 + i));
            entity.setTime(System.currentTimeMillis());
            iranAirEntities.add(entity);
        }
        return iranAirEntities;
    }
}
