package ir.esafar.interview.interview.reactive.controller;

import ir.esafar.interview.interview.reactive.URLConstant;
import ir.esafar.interview.interview.reactive.live.model.PlaneEntity;
import ir.esafar.interview.interview.reactive.live.service.ReactivePlaneService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.concurrent.ExecutionException;

/**
 * @author Mohammad Yasin Sadeghi
 */
@RestController()
public class ReactiveRest {

    //    @Autowired
    private ReactivePlaneService reactivePlaneService;

    @GetMapping(URLConstant.FLUX_ENDPOINT)
    public Flux<Integer> getFlux() {
        return Flux.just(1, 2, 3, 4).log();
    }

/*    @GetMapping(URLConstant.PLANE_LIVE_ENDPOINT)
    public Flux<PlaneEntity> getLives() throws ExecutionException, InterruptedException {
        return reactivePlaneService.getLivePlanes();
    }*/

    @GetMapping(value= URLConstant.FLUX_STREAM_ENDPOINT,
            produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
    public Flux<Long> getFluxStream() {
        return Flux.interval(Duration.ofSeconds(1))
                .log();
    }

    @GetMapping(value= URLConstant.MONO_ENDPOINT)
    public Mono<Integer> getMono() {
        return Mono.just(1)
                .log();
    }


}
