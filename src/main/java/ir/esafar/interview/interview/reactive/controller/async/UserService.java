package ir.esafar.interview.interview.reactive.controller.async;

/**
 * @author Mohammad Yasin Sadeghi
 */
public class UserService {
    public User getUserDetails(String userId) {
        return new User(userId);
    }

    public Double getCreditRating(User user) {
        return 10.5;
    }



}
