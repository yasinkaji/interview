package ir.esafar.interview.interview.reactive.live.controller;

import ir.esafar.interview.interview.reactive.URLConstant;
import ir.esafar.interview.interview.reactive.live.model.PlaneEntity;
import ir.esafar.interview.interview.reactive.live.service.ReactivePlaneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

/**
 * @author Mohammad Yasin Sadeghi
 */
@RestController(URLConstant.PLANE_ENDPOINT)
public class PlaneRest {

    @Autowired
    private ReactivePlaneService reactivePlaneService;

    @GetMapping(value = URLConstant.PLANE_LIVE_ENDPOINT, produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
    public Flux<PlaneEntity> getLives() {
        return reactivePlaneService.getLivePlanes();
    }

}
