package ir.esafar.interview.interview.reactive.live.service.mapper;

import ir.esafar.interview.interview.reactive.live.model.PlaneEntity;
import ir.esafar.interview.interview.reactive.live.model.thirdparty.IranAirEntity;
import org.springframework.stereotype.Component;

/**
 * @author Mohammad Yasin Sadeghi
 */
@Component
public class IranAirPlaneMapper extends AbstractModelMapper<IranAirEntity, PlaneEntity> {

    @Override
    public PlaneEntity map(IranAirEntity from) {
        PlaneEntity entity = null;

        if (from != null) {
            entity = new PlaneEntity();
            entity.setAirline(from.getCompany());
            entity.setId(from.getId());
            entity.setPlane(from.getFlight());
            entity.setPlaneTime(from.getTime());
        }

        return entity;
    }
}
