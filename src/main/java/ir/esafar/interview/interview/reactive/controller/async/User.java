package ir.esafar.interview.interview.reactive.controller.async;

/**
 * @author Mohammad Yasin Sadeghi
 */
public class User {
    private String userId;
    private Double creditRating;

    public User() {
    }

    public User(String userId) {
        this.userId = userId;
    }

    public User(String userId, Double creditRating) {
        this.userId = userId;
        this.creditRating = creditRating;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Double getCreditRating() {
        return creditRating;
    }

    public void setCreditRating(Double creditRating) {
        this.creditRating = creditRating;
    }
}
