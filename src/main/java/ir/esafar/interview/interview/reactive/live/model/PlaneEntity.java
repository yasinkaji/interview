package ir.esafar.interview.interview.reactive.live.model;

/**
 * @author Mohammad Yasin Sadeghi
 */
public class PlaneEntity implements Entity<Long> {

    private Long id;
    private Long planeTime;
    private String plane;
    private String airline;

    public PlaneEntity() {
    }

    @Override
    public Long getId() {
        return null;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPlaneTime() {
        return planeTime;
    }

    public void setPlaneTime(Long planeTime) {
        this.planeTime = planeTime;
    }

    public String getPlane() {
        return plane;
    }

    public void setPlane(String plane) {
        this.plane = plane;
    }

    public String getAirline() {
        return airline;
    }

    public void setAirline(String airline) {
        this.airline = airline;
    }
}
