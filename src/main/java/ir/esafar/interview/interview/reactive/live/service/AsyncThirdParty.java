package ir.esafar.interview.interview.reactive.live.service;

import ir.esafar.interview.interview.reactive.live.service.mapper.ModelMapper;
import ir.esafar.interview.interview.reactive.live.service.thirdparty.ThirdPartyService;

import java.util.List;
import java.util.function.Supplier;
/**
 * @author Mohammad Yasin Sadeghi
 */
public class AsyncThirdParty<T, F> implements Supplier<T[]> {
    private ThirdPartyService<F> thirdPartyService;
    private ModelMapper<F, T> modelMapper;

    public AsyncThirdParty(ThirdPartyService<F> thirdPartyService, ModelMapper<F, T> modelMapper) {
        this.thirdPartyService = thirdPartyService;
        this.modelMapper = modelMapper;
    }

    @Override
    public T[] get() {
        List<F> rawList = thirdPartyService.getFlightList();
        return modelMapper.mapCollection(rawList);
    }

}