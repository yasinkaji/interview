package ir.esafar.interview.interview.reactive.controller.async;

import java.util.concurrent.CompletableFuture;
import java.util.function.BiFunction;
import java.util.function.Supplier;

/**
 * @author Mohammad Yasin Sadeghi
 */
public class Main2 {

    public static void main(String[] args) throws InterruptedException {
        CompletableFuture.supplyAsync(new Supplier<Integer>() {
            @Override
            public Integer get() {
                for (int i = 0; i < 10; i++) {
                    System.out.println("i: " + i);
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                return 140;
            }
        }).thenCombine(CompletableFuture.supplyAsync(new Supplier<String>() {
            @Override
            public String get() {
                for (int j = 0; j < 10; j++) {
                    System.out.println("j: " + j);
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                return "Thread two";
            }
        }), new BiFunction<Object, Object, Object>() {
            @Override
            public Object apply(Object o, Object o2) {
                System.out.println("Object One: " + o);
                System.out.println("Object Two: " + o2);
                return "Hossein";
            }
        });

        Thread.sleep(40000);
    }
}
