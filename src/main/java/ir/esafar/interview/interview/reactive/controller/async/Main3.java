package ir.esafar.interview.interview.reactive.controller.async;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;

/**
 * @author Mohammad Yasin Sadeghi
 */
public class Main3 {

    public static void main(String[] args) throws InterruptedException {
        new Main3().test();

        Thread.sleep(4000000);
    }

    public void test() {
        String[] webPageLinks = new String[]{"1", "2", "3"};    // A list of 100 web page links
        CompletableFuture<String>[] pageContentFutures = new CompletableFuture[webPageLinks.length];
        for (int i = 0; i < webPageLinks.length; i++) {
            pageContentFutures[i] = downloadWebPage(webPageLinks[i]);
        }
// Create a combined Future using allOf()
        CompletableFuture<Void> allFutures = CompletableFuture.allOf(pageContentFutures);

        // When all the Futures are completed, call `future.join()` to get their results and collect the results in a list -
        CompletableFuture<List<String>> allPageContentsFuture = allFutures.thenApply(new Function<Void, List<String>>() {
            @Override
            public List<String> apply(Void aVoid) {
                List<String> strings = new ArrayList<>();
                for (int i = 0; i < webPageLinks.length; i++) {
                    strings.add(pageContentFutures[i].join());
                }
                System.out.println(strings);
                return strings;
            }
        });


    }


    CompletableFuture<String> downloadWebPage(String pageLink) {
        return CompletableFuture.supplyAsync(() -> {
            // Code to download and return the web page's content
            System.out.println("downloading " + pageLink);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return "page---" + pageLink;
        });
    }
}
