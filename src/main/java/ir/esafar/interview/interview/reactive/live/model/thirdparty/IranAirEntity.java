package ir.esafar.interview.interview.reactive.live.model.thirdparty;

/**
 * @author Mohammad Yasin Sadeghi
 */
public class IranAirEntity implements ThirdPartyEntity {
    private Long id;
    private Long time;
    private String flight;
    private String company;

    public IranAirEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public String getFlight() {
        return flight;
    }

    public void setFlight(String flight) {
        this.flight = flight;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }
}
