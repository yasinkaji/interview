package ir.esafar.interview.interview.reactive.controller.async;

import java.util.function.Consumer;

/**
 * @author Mohammad Yasin Sadeghi
 */
public class Aggregator implements Consumer<String> {

    @Override
    public void accept(String programmerName) {
        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Hey, " + programmerName);
    }
}
