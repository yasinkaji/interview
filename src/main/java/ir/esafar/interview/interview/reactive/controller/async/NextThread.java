package ir.esafar.interview.interview.reactive.controller.async;

import java.util.function.Function;

/**
 * @author Mohammad Yasin Sadeghi
 */
public class NextThread implements Function<String, String> {

    @Override
    public String apply(String programmerName) {
        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(programmerName);
        return "Hello " + programmerName;
    }
}
