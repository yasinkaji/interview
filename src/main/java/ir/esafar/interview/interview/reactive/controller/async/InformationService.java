package ir.esafar.interview.interview.reactive.controller.async;

import java.util.function.Supplier;

/**
 * @author Mohammad Yasin Sadeghi
 */
public class InformationService implements Supplier<String> {
    @Override
    public String get() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return "Yasin";
    }
}
