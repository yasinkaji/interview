package ir.esafar.interview.interview.reactive.live.service.mapper;


import java.util.List;

/**
 * @author Mohammad Yasin Sadeghi
 */
public interface ModelMapper<F, T> {

    T map(F from);

    T[] mapCollection(List<F> fromList);
}
