package ir.esafar.interview.interview.reactive;

/**
 * @author Mohammad Yasin Sadeghi
 */
public interface URLConstant {
    String INTERVIEW_ROOT_URL = "/interview";

    String FLUX_ENDPOINT = INTERVIEW_ROOT_URL + "/flux";
    String FLUX_STREAM_ENDPOINT = INTERVIEW_ROOT_URL + "/fluxStream";

    String MONO_ENDPOINT = INTERVIEW_ROOT_URL + "/mono";

    String FUNCTIONAL_ROOT_URL = INTERVIEW_ROOT_URL + "/functional";
    String FUNCTIONAL_FLUX_ENDPOINT = FUNCTIONAL_ROOT_URL + "/flux";
    String FUNCTIONAL_MONO_ENDPOINT = FUNCTIONAL_ROOT_URL + "/mono";

    String PLANE_ENDPOINT = "/plane";
    String PLANE_LIVE_ENDPOINT =  "/live";
}
