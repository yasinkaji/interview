package ir.esafar.interview.interview.reactive.controller.functional;

import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.HandlerFunction;
import org.springframework.web.reactive.function.server.ServerRequest;
import reactor.core.publisher.Mono;

/**
 * @author Mohammad Yasin Sadeghi
 */
@Component
public class SampleHandler implements HandlerFunction {

    @Override
    public Mono handle(ServerRequest request) {
        return null;
    }
}
