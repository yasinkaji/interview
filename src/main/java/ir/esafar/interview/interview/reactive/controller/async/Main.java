package ir.esafar.interview.interview.reactive.controller.async;

import java.util.concurrent.CompletableFuture;

/**
 * @author Mohammad Yasin Sadeghi
 */
public class Main {

    public static void main(String[] args) throws InterruptedException {
        Thread.sleep(4000);

        CompletableFuture<String> completableFuture = CompletableFuture.supplyAsync(new InformationService());
        completableFuture = completableFuture.thenApply(new NextThread());
        completableFuture.thenAccept(new Aggregator());
        completableFuture.thenRun(new Printer());

        Thread.sleep(1000);
        System.out.println("End Program");
        Thread.sleep(400000);
    }
}
