package ir.esafar.interview.interview.reactive.live.service;

import ir.esafar.interview.interview.reactive.live.model.PlaneEntity;
import reactor.core.publisher.Flux;

import java.util.concurrent.ExecutionException;

/**
 * @author Mohammad Yasin Sadeghi
 */
public interface PlaneService {

    Flux<PlaneEntity> getLivePlanes() throws ExecutionException, InterruptedException;
}
