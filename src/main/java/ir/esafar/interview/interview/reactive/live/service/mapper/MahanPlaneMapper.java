package ir.esafar.interview.interview.reactive.live.service.mapper;

import ir.esafar.interview.interview.reactive.live.model.PlaneEntity;
import ir.esafar.interview.interview.reactive.live.model.thirdparty.MahanEntity;
import org.springframework.stereotype.Component;

/**
 * @author Mohammad Yasin Sadeghi
 */
@Component
public class MahanPlaneMapper extends AbstractModelMapper<MahanEntity, PlaneEntity> {

    @Override
    public PlaneEntity map(MahanEntity from) {
        PlaneEntity entity = null;

        if (from != null) {
            entity = new PlaneEntity();
            entity.setAirline(from.getAirline());
            entity.setId(from.getIdentifier());
            entity.setPlane(from.getPlane());
            entity.setPlaneTime(from.getTime());
        }

        return entity;
    }
}
