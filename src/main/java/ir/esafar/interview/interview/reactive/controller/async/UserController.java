package ir.esafar.interview.interview.reactive.controller.async;

import java.util.concurrent.CompletableFuture;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * @author Mohammad Yasin Sadeghi
 */
public class UserController {

    private UserService userService = new UserService();

    public static void main(String[] args) {
        UserController controller = new UserController();

        CompletableFuture<User> userCompletableFuture = controller.getUsersDetail("12");
        CompletableFuture<CompletableFuture<Double>> completableFuture = userCompletableFuture.thenApply(new Function<User, CompletableFuture<Double>>() {
            @Override
            public CompletableFuture<Double> apply(User user) {
                return controller.getCreditRating(user);
            }
        });
    }

    CompletableFuture<User> getUsersDetail(String userId) {
        return CompletableFuture.supplyAsync(new Supplier<User>() {
            @Override
            public User get() {
                return userService.getUserDetails(userId);
            }
        });
    }

    CompletableFuture<Double> getCreditRating(User user) {
        return CompletableFuture.supplyAsync(new Supplier<Double>() {
            @Override
            public Double get() {
                return userService.getCreditRating(user);
            }
        });
    }
}
