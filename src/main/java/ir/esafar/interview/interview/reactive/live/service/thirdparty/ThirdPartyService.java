package ir.esafar.interview.interview.reactive.live.service.thirdparty;

import java.util.List;

/**
 * @author Mohammad Yasin Sadeghi
 */
public interface ThirdPartyService<T> {

    List<T> getFlightList();
}
