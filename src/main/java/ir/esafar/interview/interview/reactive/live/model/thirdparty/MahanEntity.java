package ir.esafar.interview.interview.reactive.live.model.thirdparty;

/**
 * @author Mohammad Yasin Sadeghi
 */
public class MahanEntity implements ThirdPartyEntity {
    private Long identifier;
    private Long time;
    private String plane;
    private String airline;

    public MahanEntity() {
    }

    public Long getIdentifier() {
        return identifier;
    }

    public void setIdentifier(Long identifier) {
        this.identifier = identifier;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public String getPlane() {
        return plane;
    }

    public void setPlane(String plane) {
        this.plane = plane;
    }

    public String getAirline() {
        return airline;
    }

    public void setAirline(String airline) {
        this.airline = airline;
    }
}
