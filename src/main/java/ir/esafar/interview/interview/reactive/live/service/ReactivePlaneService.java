package ir.esafar.interview.interview.reactive.live.service;

import ir.esafar.interview.interview.reactive.live.model.PlaneEntity;
import ir.esafar.interview.interview.reactive.live.model.thirdparty.IranAirEntity;
import ir.esafar.interview.interview.reactive.live.model.thirdparty.MahanEntity;
import ir.esafar.interview.interview.reactive.live.service.mapper.IranAirPlaneMapper;
import ir.esafar.interview.interview.reactive.live.service.mapper.MahanPlaneMapper;
import ir.esafar.interview.interview.reactive.live.service.thirdparty.IranAirWebService;
import ir.esafar.interview.interview.reactive.live.service.thirdparty.MahanWebService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.Arrays;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;

/**
 * @author Mohammad Yasin Sadeghi
 */
@Service
public class ReactivePlaneService implements PlaneService {

    @Autowired
    private IranAirWebService iranAirWebService;
    @Autowired
    private MahanWebService mahanWebService;
    @Autowired
    private IranAirPlaneMapper iranAirPlaneMapper;
    @Autowired
    private MahanPlaneMapper mahanPlaneMapper;

    @Override
    public Flux<PlaneEntity> getLivePlanes() {
        return getFastestFuture();
    }

    private Flux<PlaneEntity> getFastestFuture() {
        AsyncThirdParty<PlaneEntity, IranAirEntity> asyncIranAirService =
                new AsyncThirdParty<>(iranAirWebService, iranAirPlaneMapper);

        AsyncThirdParty<PlaneEntity, MahanEntity> asyncMahanService =
                new AsyncThirdParty<>(mahanWebService, mahanPlaneMapper);

        CompletableFuture<PlaneEntity[]> iranAirFuture = CompletableFuture.supplyAsync(asyncIranAirService);
        CompletableFuture<PlaneEntity[]> mahanFuture = CompletableFuture.supplyAsync(asyncMahanService);
        Flux<PlaneEntity> iranAirFutureFlux = getFluxFromFuture(iranAirFuture);
        Flux<PlaneEntity> mahanFutureFlux = getFluxFromFuture(mahanFuture);
        return mahanFutureFlux.mergeWith(iranAirFutureFlux).log();
    }

    private Flux<PlaneEntity> getFluxFromFuture(CompletableFuture<PlaneEntity[]> future) {
        return Mono.fromFuture(future).flatMapMany((Function<Object[], Flux<PlaneEntity>>) planeEntities -> {
            PlaneEntity[] g = Arrays.copyOf(planeEntities, planeEntities.length, PlaneEntity[].class);
            return Flux.fromArray(g).delayElements(Duration.ofSeconds(1));
        });
    }

}
