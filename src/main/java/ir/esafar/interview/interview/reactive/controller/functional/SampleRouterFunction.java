package ir.esafar.interview.interview.reactive.controller.functional;

import ir.esafar.interview.interview.reactive.URLConstant;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.*;


/**
 * @author Mohammad Yasin Sadeghi
 */
@Configuration
public class SampleRouterFunction {

    private static RequestPredicate fluxPredicate;
    private static RequestPredicate monoPredicate;

    public SampleRouterFunction() {
        fluxPredicate = RequestPredicates.GET(URLConstant.FUNCTIONAL_FLUX_ENDPOINT)
                .and(RequestPredicates.accept(MediaType.APPLICATION_JSON));

        monoPredicate = RequestPredicates.GET(URLConstant.FUNCTIONAL_MONO_ENDPOINT)
                .and(RequestPredicates.accept(MediaType.APPLICATION_JSON));
    }

    @Bean
    public RouterFunction<ServerResponse> route(SampleHandlerFunction sampleHandlerFunction) {
        return RouterFunctions
                .route(fluxPredicate, sampleHandlerFunction::flux)
                .andRoute(monoPredicate, sampleHandlerFunction::mono);
    }

}
