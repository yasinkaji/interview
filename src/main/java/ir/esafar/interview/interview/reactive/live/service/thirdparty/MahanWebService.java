package ir.esafar.interview.interview.reactive.live.service.thirdparty;

import ir.esafar.interview.interview.reactive.live.model.thirdparty.IranAirEntity;
import ir.esafar.interview.interview.reactive.live.model.thirdparty.MahanEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Mohammad Yasin Sadeghi
 */
@Service
public class MahanWebService implements ThirdPartyService<MahanEntity> {

    @Override
    public List<MahanEntity> getFlightList() {
        List<MahanEntity> mahanEntities = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            MahanEntity entity = new MahanEntity();
            entity.setAirline("Mahan");
            entity.setPlane("Plane " + i);
            entity.setIdentifier(Long.valueOf(100 + i));
            entity.setTime(System.currentTimeMillis());
            mahanEntities.add(entity);
        }
        return mahanEntities;
    }
}
