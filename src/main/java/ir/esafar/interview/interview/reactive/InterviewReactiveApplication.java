package ir.esafar.interview.interview.reactive;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InterviewReactiveApplication {

	public static void main(String[] args) {
		SpringApplication.run(InterviewReactiveApplication.class, args);
	}

}
