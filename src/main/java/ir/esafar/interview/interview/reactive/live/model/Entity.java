package ir.esafar.interview.interview.reactive.live.model;

/**
 * @author Mohammad Yasin Sadeghi
 */
public interface Entity<ID> {

    ID getId();
}
