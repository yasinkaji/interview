package ir.esafar.interview.interview.reactive.live.service.mapper;

import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * @author Mohammad Yasin Sadeghi
 */
public abstract class AbstractModelMapper<F, T> implements ModelMapper<F, T> {

    @Override
    public T[] mapCollection(List<F> fromList) {
        T[] list = (T[]) new Object[fromList.size()];
        if (!CollectionUtils.isEmpty(fromList)) {
            int i = 0;
            for (F from : fromList) {
                T to = map(from);
                list[i] = to;
                i++;
            }
        }
        return list;
    }
}
