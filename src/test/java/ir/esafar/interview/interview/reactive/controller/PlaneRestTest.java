package ir.esafar.interview.interview.reactive.controller;

import ir.esafar.interview.interview.reactive.URLConstant;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.EntityExchangeResult;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.util.Arrays;
import java.util.List;

/**
 * @author Mohammad Yasin Sadeghi
 */
@RunWith(SpringRunner.class)
@WebFluxTest
public class PlaneRestTest {
    @Autowired
    private WebTestClient webTestClient;

    @Test
    public void getFluxTest_Approach1() {

        Flux<Integer> integerFlux = webTestClient.get().uri(URLConstant.FLUX_ENDPOINT)
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .exchange()
                .expectStatus().isOk()
                .returnResult(Integer.class)
                .getResponseBody();


        StepVerifier.create(integerFlux)
                .expectSubscription()
                .expectNext(1)
                .expectNext(2)
                .expectNext(3)
                .expectNext(4)
                .verifyComplete();
    }

    @Test
    public void getFluxTest_Approach2() {

        webTestClient.get().uri(URLConstant.FLUX_ENDPOINT)
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(Integer.class)
                .hasSize(4);
    }

    @Test
    public void getFluxTest_Approach3() {

        List<Integer> expectedResult = Arrays.asList(1, 2, 3, 4);
        EntityExchangeResult<List<Integer>> result =
                webTestClient.get().uri(URLConstant.FLUX_ENDPOINT)
                        .accept(MediaType.APPLICATION_JSON_UTF8)
                        .exchange()
                        .expectStatus().isOk()
                        .expectBodyList(Integer.class)
                        .returnResult();

        Assert.assertEquals(expectedResult, result.getResponseBody());

    }

    @Test
    public void getFluxTest_Approach4() {

        List<Integer> expectedResult = Arrays.asList(1, 2, 3, 4);
        webTestClient.get().uri(URLConstant.FLUX_ENDPOINT)
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(Integer.class)
                .consumeWith((response) -> {
                    Assert.assertEquals(expectedResult, response.getResponseBody());

                });

    }

    @Test
    public void getFluxStreamTest_Approach1() {


        Flux<Long> longStreamFlux =
                webTestClient.get().uri(URLConstant.FLUX_STREAM_ENDPOINT)
                .accept(MediaType.APPLICATION_STREAM_JSON)
                .exchange()
                .expectStatus().isOk()
                .returnResult(Long.class)
                .getResponseBody();


        StepVerifier.create(longStreamFlux)
                .expectNext(0l)
                .expectNext(1l)
                .expectNext(2l)
                .thenCancel()
                .verify();
    }













    @Test
    public void getMonoTest() {

        Integer expectedValue = 1;
        webTestClient.get().uri(URLConstant.MONO_ENDPOINT)
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .exchange()
                .expectStatus().isOk()
                .expectBody(Integer.class)
                .consumeWith((response) -> {
                    Assert.assertEquals(expectedValue, response.getResponseBody());

                });

    }

}
