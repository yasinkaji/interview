package ir.esafar.interview.interview.reactive.controller.functional;

import org.junit.Test;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * @author Mohammad Yasin Sadeghi
 */
public class CompletableFutureTest {
    @Test
    public void get_forever_block_test() throws ExecutionException, InterruptedException {
        System.out.println("here");
        CompletableFuture<Integer> completableFuture = new CompletableFuture<>();
        completableFuture.get();
    }

    @Test
    public void complete_manually_block_test() throws ExecutionException, InterruptedException {
        System.out.println("here");
        CompletableFuture<Integer> completableFuture = new CompletableFuture<>();

        Thread.sleep(2000);
        boolean isCompleted = false;
        if (!completableFuture.isDone())
            isCompleted = completableFuture.complete(15);

        System.out.println("isCompleted: " + isCompleted);
        System.out.println("Result: " + completableFuture.get());
    }

    @Test
    public void runAsync_test() throws ExecutionException, InterruptedException {
        System.out.println("here");
        CompletableFuture<Void> completableFuture = CompletableFuture.runAsync(() -> {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("background long time task is finished.");
        });

        boolean isCompleted = false;
        if (!completableFuture.isDone())
            isCompleted = completableFuture.complete(null);

        System.out.println("isCompleted: " + isCompleted);
        System.out.println("Result: " + completableFuture.get());
    }

    @Test
    public void supplyAsync_test() {
        CompletableFuture.supplyAsync(() -> "Mohammad Yasin");
    }

}
