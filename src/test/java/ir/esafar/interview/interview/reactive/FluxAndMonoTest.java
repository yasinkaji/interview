package ir.esafar.interview.interview.reactive;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.function.Consumer;

/**
 * @author Mohammad Yasin Sadeghi
 */
public class FluxAndMonoTest {
    @Test
    public void fluxTest() {
        Flux<String> stringFlux = Flux.just("Spring", "Spring Boot", "Reactive Spring")
                .concatWith(Flux.error(new RuntimeException("Hey Programmer!"))).log()
                .concatWith(Flux.just("Hibernate"));

        stringFlux.subscribe(new Consumer<String>() {
            @Override
            public void accept(String s) {
                System.out.println(s);
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) {
                System.err.println("Hey Yasin. ");
                System.err.println(throwable.getMessage());
            }
        });
    }

    @Test
    public void fluxTestElements_WithoutError() {
        Flux<String> stringFlux = Flux.just("1", "2", "3")
                .log();

        StepVerifier.create(stringFlux)
                .expectNext("1")
                .expectNext("2")
                .expectNext("3")
                .verifyComplete();

    }

    @Test
    public void testMono() {
        Mono<String> stringMono = Mono.just("1");

        StepVerifier.create(stringMono.log())
                .expectNext("1")
                .verifyComplete();
    }


    @Test
    public void testMono_withError() {
        Mono<String> stringMono = Mono.error(new RuntimeException("Hello Error!"));

        StepVerifier.create(stringMono.log())
                .expectError(RuntimeException.class)
                .verify();
    }


}
