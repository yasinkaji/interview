package ir.esafar.interview.interview.reactive.controller.functional;

import ir.esafar.interview.interview.reactive.URLConstant;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

/**
 * @author Mohammad Yasin Sadeghi
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureWebTestClient
public class SampleHandlerTest {

    @Autowired
    private WebTestClient webTestClient;

    @Test
    public void getFluxTest() {

        Flux<Integer> integerFlux = webTestClient.get().uri(URLConstant.FUNCTIONAL_FLUX_ENDPOINT)
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .exchange()
                .expectStatus().isOk()
                .returnResult(Integer.class)
                .getResponseBody();


        StepVerifier.create(integerFlux)
                .expectSubscription()
                .expectNext(1)
                .expectNext(2)
                .expectNext(3)
                .expectNext(4)
                .verifyComplete();
    }
    @Test
    public void getMonoTest() {

        Integer expectedValue = 1;
        webTestClient.get().uri(URLConstant.FUNCTIONAL_MONO_ENDPOINT)
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .exchange()
                .expectStatus().isOk()
                .expectBody(Integer.class)
                .consumeWith((response) -> {
                    Assert.assertEquals(expectedValue, response.getResponseBody());

                });

    }
}
