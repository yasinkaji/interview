
package ir.esafar.interview.interview.reactive.controller.functional;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;

public class InvokeAnyExample {
    public static void main(String[] args) throws InterruptedException, ExecutionException {
        ExecutorService executorService = Executors.newFixedThreadPool(5);

        Callable<String> task1 = () -> {
            Thread.sleep(20000);
            return "Result of Task1";
        };

        Callable<String> task2 = () -> {
            Thread.sleep(10000);
            return "Result of Task2";
        };

        Callable<String> task3 = () -> {
            Thread.sleep(50000);
            return "Result of Task3";
        };

        // Returns the result of the fastest callable. (task2 in this case)
       executorService.submit(task1);

        System.out.println("ended");

        executorService.shutdown();
    }
}